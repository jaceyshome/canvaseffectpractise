<?php
class Usermodel extends CI_Model {
    function Usermodel()
    {
        parent::__construct();
		require_once "application/vo/User.php";
		require_once "application/vo/Teacher.php";
		require_once "application/vo/Options.php";
		$this->load->database();
		$this->load->library('TimeUtil');
		$this->load->model('commentmodel');
    }
	
	function getUserById($id)
	{
		$results =  $this->getUsers(array("userId" => $id));
		if (count($results) == 0) return false;
		return $results[0];
	}
	
	function getUsers($options = null) {
		$options = new Options($options);
		$userId = $options->getOption("userId", false);
		$userUserName = $options->getOption("userUserName", false);
		$this->db->select("*");
		$this->db->from("view_user");
		if ($userId !== false) $this->db->where("user_id", $userId);
		if ($userUserName !== false) $this->db->where("user_userName", $userUserName);
		$query = $this->db->get();
		$users = array();
		foreach($query->result() as $row) {
			$users [] = User::fromRow($row);
		}
		return $users;
	}
	
	
	function getUserFromCredentials($user) {
		
		//TODO this is related to session, but is it good to pass all admin information, but I only need userId
		$results = $this->getUsers(array("userId" => $user->id));
		if (count($results) == 0) return false;
		return $results[0];
	}
	
	function listUserCompleteTeacherModules($userId) {
		$sql = "
		
			SELECT 	resourcebank_id,
					resourcebank_name,
					resourcebank_thumbnailLink,
					resourcebank_languageId,
					userId,
					favouriteFlag,	
					ownerId,
					isPublic,
					isCompleted AS completed
					
			FROM
			(
				SELECT * 

				FROM
					(
						SELECT 	
							resourcebank_id AS resourcebankId,
							favouriteFlag,	
							userId,
							userresource_ownerId AS ownerId,
							userresource_public AS isPublic,
							isCompleted

						FROM resourcebank

						LEFT JOIN
						(
							SELECT
								userresource_resourcebankId,
								userresource_ownerId,
								userresource_public 
							FROM userresource 
							WHERE userresource_ownerId = userresource_userId
						) AS tr_owner
						ON resourcebank_id = userresource_resourcebankId

						LEFT JOIN 
						(
							SELECT 
								userresource_favouriteFlag AS favouriteFlag,
								userresource_resourcebankId AS tr_resourcebankId,
								userresource_userId AS userId,
								userresource_completed  AS isCompleted   
							FROM userresource
							WHERE userresource_userId = '$userId'
						) AS tr_user
						ON resourcebank_id = tr_resourcebankId
				  
					) AS t_owner_resources

				WHERE userId = ownerId
				 
				UNION

				SELECT *

				FROM
					(
						SELECT 	
							resourcebank_id AS resourcebankId,
							favouriteFlag,	
							userId,
							userresource_ownerId AS ownerId,
							userresource_public AS isPublic,
							isCompleted

						FROM resourcebank

						LEFT JOIN
						(
							SELECT
								userresource_resourcebankId,
								userresource_ownerId,
								userresource_public 
							FROM userresource 
							WHERE userresource_ownerId = userresource_userId
						) AS tr_owner
						ON resourcebank_id = userresource_resourcebankId

						LEFT JOIN 
						(
							SELECT 
								userresource_favouriteFlag AS favouriteFlag,
								userresource_resourcebankId AS tr_resourcebankId,
								userresource_userId AS userId,
								userresource_completed  AS isCompleted   
							FROM userresource
							WHERE userresource_userId = '$userId'
						) AS tr_user
						ON resourcebank_id = tr_resourcebankId
				  
					) AS t_public_resources
				WHERE 
				isPublic = 1
				OR isPublic IS NULL
				OR ownerId IS NULL

				ORDER BY userId DESC
			) AS t_resources
			JOIN view_resourcebank
			ON resourcebank_id = resourcebankId
			WHERE resourcebank_resourceTypeId = 4
			AND isCompleted = 1

		";
		//NOTE: The last line: resourcebank_resourceTypeId IN a list, only shows resource resourceTypeId is in that list
		
		//TODO store procedure
		//$sql = "CALL listTeacherPublicResources(?)";
		$query = $this->db->query($sql);
		
		//TODO need to double check the return value, if it can return false
		if (!$query->result_id) {
			return false;
		}
		$results = array();
		foreach($query->result() as $row) {	
			$teacherModule = ResourceBank::fromRow($row);
			$teacherModule-> sectionTotal = $this->getResourceBankSectionsTotal($teacherModule-> id);
			$results [] =  $teacherModule;
		}
		return $results;
	
	}
	
	function getResourceBankSectionsTotal($resourceBankId) {
		//TODO storeProcedure getTeacherModuleInfo
		$sql = "
			SELECT COUNT(*) AS sectionTotal
			FROM
				(
					SELECT
						*
					FROM 
						resourcebanksection
					JOIN resourcebank
					ON resourcebanksection_resourceBankId = resourcebank_id
					WHERE resourcebanksection_resourceBankId = '$resourceBankId'
				) AS moduelsectionCount
		";
		
		$query = $this->db->query($sql);
		
		//TODO need to double check the return value, if it can return false
		if (!$query->result_id) {
			return false;
		}
		
		$results = array();
		foreach($query->result() as $row) {
			$results [] = $row;
		}
		return ($results[0]->sectionTotal);
	}
	
	function listUserFavouriteResources($userId) {
		$sql = "
		
			SELECT 	resourcebank_id,
					resourcebank_name,
					resourcebank_thumbnailLink,
					resourcebank_languageId,
					userId,
					favouriteFlag,	
					ownerId,
					isPublic,
					isCompleted AS completed
					
			FROM
			(
				SELECT * 

				FROM
					(
						SELECT 	
							resourcebank_id AS resourcebankId,
							favouriteFlag,	
							userId,
							userresource_ownerId AS ownerId,
							userresource_public AS isPublic,
							isCompleted

						FROM resourcebank

						LEFT JOIN
						(
							SELECT
								userresource_resourcebankId,
								userresource_ownerId,
								userresource_public 
							FROM userresource 
							WHERE userresource_ownerId = userresource_userId
						) AS tr_owner
						ON resourcebank_id = userresource_resourcebankId

						LEFT JOIN 
						(
							SELECT 
								userresource_favouriteFlag AS favouriteFlag,
								userresource_resourcebankId AS tr_resourcebankId,
								userresource_userId AS userId,
								userresource_completed  AS isCompleted   
							FROM userresource
							WHERE userresource_userId = '$userId'
						) AS tr_user
						ON resourcebank_id = tr_resourcebankId
				  
					) AS t_owner_resources

				WHERE userId = ownerId
				 
				UNION

				SELECT *

				FROM
					(
						SELECT 	
							resourcebank_id AS resourcebankId,
							favouriteFlag,	
							userId,
							userresource_ownerId AS ownerId,
							userresource_public AS isPublic,
							isCompleted

						FROM resourcebank

						LEFT JOIN
						(
							SELECT
								userresource_resourcebankId,
								userresource_ownerId,
								userresource_public 
							FROM userresource 
							WHERE userresource_ownerId = userresource_userId
						) AS tr_owner
						ON resourcebank_id = userresource_resourcebankId

						LEFT JOIN 
						(
							SELECT 
								userresource_favouriteFlag AS favouriteFlag,
								userresource_resourcebankId AS tr_resourcebankId,
								userresource_userId AS userId,
								userresource_completed  AS isCompleted   
							FROM userresource
							WHERE userresource_userId = '$userId'
						) AS tr_user
						ON resourcebank_id = tr_resourcebankId
				  
					) AS t_public_resources
				WHERE 
				isPublic = 1
				OR isPublic IS NULL
				OR ownerId IS NULL

				ORDER BY userId DESC
			) AS t_resources
			JOIN view_resourcebank
			ON resourcebank_id = resourcebankId
			WHERE resourcebank_resourceTypeId IN ( 7, 8, 9, 10)
			AND favouriteFlag = 1

		";
		//NOTE: The last line: resourcebank_resourceTypeId IN a list, only shows resource resourceTypeId is in that list
		
		//TODO store procedure
		//$sql = "CALL listTeacherPublicResources(?)";
		$query = $this->db->query($sql);
		
		//TODO need to double check the return value, if it can return false
		if (!$query->result_id) {
			return false;
		}
		
		$resources = array();
		foreach($query->result() as $row) {	
			$resources [] = ResourceBank::fromRow($row);
		}
		return $resources;
	
	}
	
	function listUserPublicResources($userId) {
	
		$sql = "
		
			SELECT 	resourcebank_id,
					resourcebank_name,
					resourcebank_thumbnailLink,
					resourcebank_languageId,
					userId,
					favouriteFlag,	
					ownerId,
					isPublic,
					isCompleted AS completed
					
			FROM
			(
				SELECT * 

				FROM
					(
						SELECT 	
							resourcebank_id AS resourcebankId,
							favouriteFlag,	
							userId,
							userresource_ownerId AS ownerId,
							userresource_public AS isPublic,
							isCompleted

						FROM resourcebank

						LEFT JOIN
						(
							SELECT
								userresource_resourcebankId,
								userresource_ownerId,
								userresource_public 
							FROM userresource 
							WHERE userresource_ownerId = userresource_userId
						) AS tr_owner
						ON resourcebank_id = userresource_resourcebankId

						LEFT JOIN 
						(
							SELECT 
								userresource_favouriteFlag AS favouriteFlag,
								userresource_resourcebankId AS tr_resourcebankId,
								userresource_userId AS userId,
								userresource_completed  AS isCompleted   
							FROM userresource
							WHERE userresource_userId = '$userId'
						) AS tr_user
						ON resourcebank_id = tr_resourcebankId
				  
					) AS t_owner_resources

				WHERE userId = ownerId
				 
				UNION

				SELECT *

				FROM
					(
						SELECT 	
							resourcebank_id AS resourcebankId,
							favouriteFlag,	
							userId,
							userresource_ownerId AS ownerId,
							userresource_public AS isPublic,
							isCompleted

						FROM resourcebank

						LEFT JOIN
						(
							SELECT
								userresource_resourcebankId,
								userresource_ownerId,
								userresource_public 
							FROM userresource 
							WHERE userresource_ownerId = userresource_userId
						) AS tr_owner
						ON resourcebank_id = userresource_resourcebankId

						LEFT JOIN 
						(
							SELECT 
								userresource_favouriteFlag AS favouriteFlag,
								userresource_resourcebankId AS tr_resourcebankId,
								userresource_userId AS userId,
								userresource_completed  AS isCompleted   
							FROM userresource
							WHERE userresource_userId = '$userId'
						) AS tr_user
						ON resourcebank_id = tr_resourcebankId
				  
					) AS t_public_resources
				WHERE 
				isPublic = 1
				OR isPublic IS NULL
				OR ownerId IS NULL

				ORDER BY userId DESC
			) AS t_resources
			JOIN view_resourcebank
			ON resourcebank_id = resourcebankId
			WHERE resourcebank_resourceTypeId IN (7,8,9,10,11)

		";
		//NOTE: The last line: resourcebank_resourceTypeId IN a list, only shows resource resourceTypeId is in that list
		
		//TODO store procedure
		//$sql = "CALL listTeacherPublicResources(?)";
		$query = $this->db->query($sql);
		
		//TODO need to double check the return value, if it can return false
		if (!$query->result_id) {
			return false;
		}
		
		$resources = array();
		foreach($query->result() as $row) {	
			
			$resources [] = ResourceBank::fromRow($row);
			 
		}
		return $resources;
	}
	
	
	function listUserAllResources($userId) {
		
		//TODO resource type need to be dynamic
		$sql = "
		
			SELECT * FROM view_resourcebank
			WHERE resourcebank_resourceTypeId IN (1,2,3,4,7,8,9,11,15,16,17)

		";
		//it is only for admin can access this function
		$query = $this->db->query($sql);
		
		//TODO need to double check the return value, if it can return false
		if (!$query->result_id) {
			return false;
		}
		
		$resources = array();
		foreach($query->result() as $row) {	
			$resources [] = ResourceBank::fromRow($row);
		}
		return $resources;
	}
	
	function insertUserUploadResource($userresource) {
		$result = 0;
		$data = array(
			'userresource_resourceBankId' => $userresource->id,
			'userresource_public' => $userresource->isPublic,
			'userresource_userId' => $userresource-> userId,
			'userresource_ownerId' => $userresource-> userId,
			'userresource_favouriteFlag' => 1,
			'userresource_completed' => 0,
			'userresource_uploaded' => 1

		);
		$this->db->insert('userresource', $data);
		return $this->db->insert_id();
	}
	
	public function getUserResources($options = null) {		
		$options = new Options($options);
		
		$userId = $options->getOption("userId", false);
		$resourceBankId = $options->getOption("resourceBankId", false);
		$resourceTypeId = $options->getOption("resourceTypeId", false);
		$resourceTypeName = $options->getOption("resourceTypeName", false);
		$schoolYearId = $options->getOption("schoolYearId", false);
		$schoolYearName = $options->getOption("schoolYearName", false);
		$ownerId = $options->getOption("ownerId", false);
		
		$this->db->select('*');	
		$this->db->from('userresource');
		$this->db->join('view_resourcebank', 'userresource_resourcebankId = resourcebank_id');
		
		if ($ownerId !== false) $this->db->where("userresource_userId = userresource_ownerId");
		if ($userId !== false) $this->db->where("userresource_userId", $userId);
		if ($resourceBankId !== false) $this->db->where("resourceBank_Id", $resourceBankId);
		if ($resourceTypeId !== false) $this->db->where("resourceBank_resourceTypeId", $resourceTypeId);
		if ($resourceTypeName !== false) $this->db->where("resourcetype_name", $resourceTypeName);
		if ($schoolYearId !== false) $this->db->where("resourcebank_schoolYearId", $schoolYearId);
		if ($schoolYearName !== false) $this->db->where("schoolyear_name", $schoolYearName);
		
		$query = $this->db->get();
		$resources = array();
		foreach($query->result() as $row) {
			$resource = ResourceBank::fromRow($row);
			$param = array(
				'userId' => $userId, 
				'resourceBankId' => $resource-> id,
			);
			$resource-> comments = $this->commentmodel->getComments($param);
			array_push($resources, $resource);
		}
		
		return $resources;
	}
	
	function getUserResource($options = null) {
		$userresources = $this->getUserResources($options);
		if (count($userresources) == 0) return false;
		return $userresources[0];
	}
	
	//Owner resource can change public
	function updateUserOwnerResource($userResource) {
		$data = array();
		if (isset($userResource->isPublic))$data['userresource_public'] = $userResource->isPublic;
		if (isset($userResource->favouriteFlag))$data['userresource_favouriteFlag'] = $userResource->favouriteFlag;
		$data['userresource_editDateTime'] =  TimeUtil::getTime();
		$this->db->where('userresource_resourcebankId', $userResource->id);
		$this->db->where('userresource_userId', $userResource->userId);
		$this->db->where('userresource_userId = userresource_ownerId');
		$this->db->update('userresource', $data);
		
		return $this->getUserResource(array("userId"=>$userResource->userId, "resourceBankId"=>$userResource->id, "ownerId"=>$userResource->userId));
	}
	
	//Public resource only change flag
	function updateUserPublicResource($userResource) {
		$data = array();
		if (isset($userResource-> favouriteFlag))$data['userresource_favouriteFlag'] = $userResource-> favouriteFlag;; 
		$data['userresource_editDateTime'] =  TimeUtil::getTime();
		$this->db->where('userresource_resourcebankId', $userResource->id);
		$this->db->where('userresource_userId', $userResource-> userId);
		$this->db->update('userresource', $data);
		
		return $this->getUserResource(array("userId"=>$userResource->userId, "resourceBankId"=>$userResource->id));
	}
	
	function insertUserPublicResource($userResource) {
		$data = array(
			'userresource_favouriteFlag' => 1,
			'userresource_resourceBankId' => $userResource->id,
			'userresource_userId' => $userResource->userId,
			'userresource_ownerId' => $userResource->ownerId,
			'userresource_editDateTime' => TimeUtil::getTime(),
		);
		$this->db->insert('userresource', $data);
		//$insert_id = $this->db->insert_id();
		return $this->getUserResource(array("userId"=>$userResource->userId, "resourceBankId"=>$userResource->id));
	}
	
	function deleteUserPublicResource($userResource) {
		$this->db->delete('userresource', array('userresource_resourceBankId' => $userResource->id, 'userresource_userId' => $userResource->userId));
		return $this->db->affected_rows() > 0;
	}
	
	function addUserContact($userId, $contactId) {
		$this->db->select("teachercontact.*");
		$this->db->where("teachercontact_userId", $userId);
		$this->db->where("teachercontact_contactId", $contactId);
		$query = $this->db->get("teachercontact");
		
		//var_dump($contact);
		//die($contact);
		//k;
		if ($query->num_rows() > 0) {
			return false;
		}
		
		$data = array(
			'teachercontact_userId' => $userId,
			'teachercontact_contactId' => $contactId,
		);
		$this->db->insert('teachercontact', $data);
		
		return $this->getUserById($contactId);
	}
	
	function listUserUnconnects($userId) {
		$sql = "
		
			SELECT *
			FROM
				(	
					SELECT * 
					FROM teacher
					WHERE teacher_id != '$userId'
				) AS OtherTeacher
			LEFT JOIN 
				(	SELECT * 
					FROM teachercontact
					WHERE teachercontact_userId = '$userId'
				) AS UserConnect
			ON UserConnect.teachercontact_contactId = OtherTeacher.teacher_id
			WHERE teachercontact_contactId IS NULL
		";
		
		$query = $this->db->query($sql);
		
		$users = array();
		foreach($query->result() as $row) {	
			$users [] = Teacher::fromRow($row);
		}
		return $users;
	}
	
	function listUserContacts($userId) {
		$sql = "
		
			SELECT *
			FROM
				(	
					SELECT * 
					FROM teacher
					WHERE teacher_id != '$userId'
				) AS OtherTeacher
			LEFT JOIN 
				(	SELECT * 
					FROM teachercontact
					WHERE teachercontact_userId = '$userId'
				) AS UserConnect
			ON UserConnect.teachercontact_contactId = OtherTeacher.teacher_id
			WHERE teachercontact_contactId IS NOT NULL
		";
		
		$query = $this->db->query($sql);

		$users = array();
		foreach($query->result() as $row) {	
			$users [] = Teacher::fromRow($row);
		}
		return $users;
	}
	
	public function listAllContacts($userId) {
		$sql = "SELECT * FROM teacher LEFT JOIN teachercontact ON teachercontact.teachercontact_contactId = teacher.teacher_id";
		$query = $this->db->query($sql);
		$contacts = array();
		foreach($query->result() as $row) {	
			$contact = Teacher::fromRow($row);
			if ($row->teachercontact_userId == $userId) {
				$contact->isAContact = true;
			}
			array_push($contacts, $contact);
		}
		return $contacts;
	}
	
	function deleteUserContact($userId, $contactId){
		$this->db->delete('teachercontact', array('teachercontact_userId' => $userId, 'teachercontact_contactId' => $contactId));
		return $this->getUserById($contactId);

	}
	
	function updateUser($user){
		$data = array();
		if (isset($user->userName))$data['user_userName'] = $user->userName;
		if (isset($user->surName))$data['user_surName'] = $user->surName;
		if (isset($user->firstName))$data['user_firstName'] = $user->firstName;
		if (isset($user->agentName))$data['user_agentName'] = $user->agentName;
		if (isset($user->dateOfBirth))$data['user_dateOfBirth'] = $user->dateOfBirth;
		if (isset($user->email))$data['user_email'] = $user->email;
		if (isset($user->password))$data['user_password'] = $user->password;
		if (isset($user->thumbnailLink))$data['user_thumbnailLink'] = $user->thumbnailLink;
		if (isset($user->skypeId))$data['user_skypeId'] = $user->skypeId;
		if (isset($user->welinkId))$data['user_welinkId'] = $user->welinkId;
		if (isset($user->locusId))$data['user_locusId'] = $user->locusId;
		if (isset($user->scootleId))$data['user_scootleId'] = $user->scootleId;
		if (isset($user->currentSchool))$data['user_currentSchool'] = $user->currentSchool;
		if (isset($user->displayInformation))$data['user_displayInformation'] = $user->displayInformation;
		$data['user_editDateTime'] = TimeUtil::getTime();
		$this->db->where('user_id', $user->id);
		$this->db->update('user', $data);
		return $this->getUserById($user->id);
	}
}
?>

