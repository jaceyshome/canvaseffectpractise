<?php
class ThumbnailGenerator
{
	
	static function generate($mediaUrl)
	{
		$CI =& get_instance();
		$CI->load->library('mediastore');
		$CI->load->library('simpleImage');
		
		$simpleimage = $CI->simpleimage;
		$mediastore = $CI->mediastore;
		
		$mediaFile = tempnam("assets/tmp", "image_");
		
		if ($mediastore->getObject($mediaUrl, $mediaFile) === false)
			return false;
			
		list($width, $height, $type, $attr) = getimagesize($mediaFile);
		
		if ($width * $height > 2400*2400)
		{
			unlink($mediaFile);
			return false;
		}
		
		$simpleimage->load($mediaFile);
		$simpleimage->resizeAndCrop(THUMBNAIL_WIDTH, THUMBNAIL_HEIGHT);
		$thumbnailUrl = "thumbnail_".$mediaUrl;
		//$thumbnailFile = "/assets/tmp/".$thumbnailUrl;
		$thumbnailFile = tempnam("/assets/tmp", "image_");
		$simpleimage->save($thumbnailFile);
		$mediastore->inputFile($thumbnailFile, $thumbnailUrl);
		
		unlink($mediaFile);
		unlink($thumbnailFile);
		return $thumbnailUrl;
	}
}
?>