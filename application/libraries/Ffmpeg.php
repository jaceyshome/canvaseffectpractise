<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Ffmpeg {

	function make_jpg($input, $output, $fromdurasec="01") {
		$CI =& get_instance();
		$ffmpegpath = $CI->config->item("ffmpegpath");
		//var_dump(file_exists($input));
		if(!file_exists($input)) return false;
		$command = "$ffmpegpath -i $input -an -ss 00:00:$fromdurasec -r 1 -vframes 1 -f mjpeg -y $output";
		//$command = "$this->ffmpegpath -i $input $output";

		exec( $command, $ret );
		//var_dump($ret);
		if(!file_exists($output)) return false;
		if(filesize($output)==0) return false;
		return true;
	}
	function getDuration($input){
		$CI =& get_instance();
		$ffmpegpath = $CI->config->item("ffmpegpath");
		if(!file_exists($input)) return false;
		$command = "$ffmpegpath -i $input 2>&1";
		ob_start();
		passthru( $command );
		$duration = ob_get_contents();
		ob_end_clean();
		//die(var_dump($duration));
		$search='/Duration: (.*?)[,]/';
		//die(var_dump($duration));
		$duration=preg_match($search, $duration, $matches, PREG_OFFSET_CAPTURE);
		//die(var_dump($duration));
		$duration = $matches[1][0];
		//die(var_dump($duration));
		$hours = (int) substr($duration, 0, 2);
		$mins = (int) substr($duration, 3, 2) + $hours * 60;
		$secs = (int) substr($duration, 6, 2) + $mins * 60;
		$secs += ((int) substr($duration, 9, 2)) / 100;
		//die(var_dump($duration));
		//die(var_dump(substr($duration, 9, 2)));
		$ms = $secs * 1000;
		//die(var_dump($ms));
		return $ms;
	}
}