<?php
class JSONOutput {
	function output($data){
		$CI =& get_instance();
		$CI->output->set_header('Content-type: application/json');
		$CI->output->set_output($this->toJSON($data));
	}
	function toJSON($data) {
		$type = gettype($data);
		switch ($type) { 
			case "integer":
			case "double":
			case "string":
			case "boolean":
				return (string)$data;
			break;
			default:
				$outputObject = $this->toObject($data);
				return json_encode($outputObject);
			break;
		}
	}
	function toObject($data) {
		$outputObject = array();
		foreach($data as $key => $value) {
			if (is_array($value)) {
				$outputObject[$key] = $this->toObject($value);
			}else if (is_object($value)) {
				$outputObject[$key] = $this->toObject($value);
			}else {
				if (is_null($value)) {
					//ignore null objects
				}else {
					$outputObject[$key] = $value;
				}
			}
		}
		return $outputObject;
	}
}
?>