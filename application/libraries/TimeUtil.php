<?php
class TimeUtil {

	static function getTime($format = 'Y-m-d H:i:s') {
		date_default_timezone_set('GMT');
		$date = date($format);
		return $date;
	}
	
	static function diffTime($time1, $time2) {
		date_default_timezone_set('GMT');
		$time1 = strtotime($time1);
		$time2 = strtotime($time2);
		$diff = $time1 - $time2;
		return $diff;
	}
	
	static function fromTime($time) {
		date_default_timezone_set('GMT');
		$time = strtotime($time);
		$date = date('Y-m-d H:i:s', $time);
		return $date;
	}

}
?>