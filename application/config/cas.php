<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

$config['cas_server_url'] = 'https://cas.uat.scootle.edu.au/cas'; 
$config['phpcas_path'] = 'application/libraries/casphp/'; 
$config['cas_disable_server_validation'] = TRUE; 
$config['cas_debug'] = TRUE; // <-- use this to enable phpCAS debug mode