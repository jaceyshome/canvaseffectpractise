<?php
/*
Public domain PHP REST Controller Base Class.
Originally by Jan Lehnardt <jan@php.net>
Usage instructions: http://jan.prima.de/~jan/plok/archives/115-REST-Controller-for-PHP-Applications.html
*/
class MY_Controller extends CI_Controller
{
    var $method = 'get'; //default
    var $debug = false;
    function MY_Controller()
    {
        parent::__construct();
		$this->load->library('JSONOutput');
		parse_str($_SERVER['QUERY_STRING'], $parameters);
		if (isset($parameters['debug']))$this->debug = true;
		if ($this->debug) {
			$this->output->enable_profiler(true);
			header("content-type: text/html");
		}
    }

	function _dispatch($id = false)
	{
		//no cache
		$this->nocache();
		if(isset($_SERVER['REQUEST_METHOD']))
			$this->method = strToLower($_SERVER["REQUEST_METHOD"]);
		
		if (isset($_POST['_method']))
			$dispatch_method = "_{$_POST['_method']}";
		else
			$dispatch_method = "_{$this->method}";
		
		if(!is_callable(array($this, $dispatch_method))) {
			$this->_respondError("501 Not Implemented");
			return false;
		}
		$this->{ $dispatch_method } ($id);
	}
	
    function _getProtocol()
    {
        $protocol = "HTTP/1.1";
        if(isset($_SERVER['SERVER_PROTOCOL'])) {
            $protocol = $_SERVER['SERVER_PROTOCOL'];
        }
        return $protocol;
    }

    function _respondError($error)
    {
        $protocol = $this->_getProtocol();
        header("$protocol $error");
    }

    function _respond($body, $headers = array())
    {
        foreach($headers AS $header => $value) {
            header("{$header}: {$value}");
        }
        echo $body;
    }
	
	function _get($id) {
		$this->returnError(405, "Method Not Allowed", "You may not call GET on this end point. Consult the user guide for more information");
	}
	function _post($id){
		$this->returnError(405, "Method Not Allowed", "You may not call POST on this end point. Consult the user guide for more information");
	}
	function _put($id){
		$this->returnError(405, "Method Not Allowed", "You may not call PUT on this end point. Consult the user guide for more information");
	}
	function _delete($id){
		$this->returnError(405, "Method Not Allowed", "You may not call DELETE on this end point. Consult the user guide for more information");
	}
	
	function returnError($code, $message, $description = null) {
		
		parse_str($_SERVER['QUERY_STRING'], $parameters);
		if(!isset($parameters['nostatus']))
			$this->output->set_header('HTTP/1.1 '.$code.' '.$message);
		
		$error = array(
			'code' => $code,
			'message' => $message,
		);
		
		if (isset($description))
			$error['description'] = $description;
			
		$this->jsonoutput->output(array('error'=>$error));
	}
	function nocache(){
		header("Cache-Control: no-cache, must-revalidate");
		header("Pragma: no-cache");
		header("Expires: Sat, 26 Jul 1997 05:00:00 GMT");
	}
}