define(['angular', 'appmodule', 'jquery'], function($compileProvider) {
	//console.log("define compile");
    var appModule = angular.module('app');
    return appModule.directive('llsCompile', function($compile) {
		//console.log("init compile"  );
		
		return {   
			scope: true,
			link: function ( scope, element, attrs ) {
			  var el;

			  attrs.$observe( 'template', function ( tpl ) {
				//console.log("template change ", tpl);
				if ( angular.isDefined( tpl ) ) {
				
				  //functions to replace <video string>
				 
				  tpl = loadEmbedVideo(tpl);
				 
				  // compile the provided template against the current scope
				  el = $compile( tpl )( scope );

				  // stupid way of emptying the element
				  element.html("");

				  // add the template content
				  element.append( el );
				  
				  //console.log(el);
				    
				  
				}
				
			  });
			  
			  
			  //[mediaid=c783e869-602b-4a11-9a51-c96ddb1eec54]
			  
				var loadEmbedVideo = function(inputString, oldSubStrMarker, length, newSubStr){
						var isComplete = false;
						var maxCounting = 10;
						var counter = 0;
						var startMarker = "[mediaid="; 
						var endMarker = "]";
						var oldStrLength = 61;
						var idLength = 36;
						var mediaTag = "<span class='video-placehold' lls-embed-video mediaid='[mediaId]'></span>";
						var newSubStr = '';
						var oldSubStr = '';
						var mediaId = 0;
												

						
						do
						{
							
							//console.log("search string");
							var startIndex = inputString.indexOf(startMarker);
							var endIndex =  inputString.indexOf(endMarker, startIndex);
							//oldStrLength =(endTabIndex + 11) -  index ;
							//idLength =  endTabIndex - (index + 16);
							//console.log("endTabIndex: "+ endTabIndex);
							//console.log("idLength: "+ idLength);
							
							if(startIndex != -1){
								//console.log("inputString", inputString);
								//console.log("find the index : "+ index);
								//console.log("get subString");
								//oldSubStr = inputString.substr(index, oldStrLength);
								mediaId = inputString.substring( startIndex+startMarker.length, endIndex);
								//console.log("mediaId ======|" + mediaId);
								var startString = inputString.substring(0,startIndex);
								var endString = inputString.substring(endIndex+endMarker.length)
								inputString = startString+mediaTag.split("[mediaId]").join(mediaId)+endString
								//console.log("New String!!!!:" + inputString);
								//console.log("------------------------------------------------------------");
								
							}else{
							
								isComplete = true;
							}
							
							counter++;
							if(counter >= maxCounting)
							{
								isComplete = true;
								break;
							}
							
							//isComplete = true;	
						} 
						while (!isComplete);
						
						return inputString;
					
						
				}
			  
			
			}
			 
			
		};
	});
	
	
	
	
	
	
});


