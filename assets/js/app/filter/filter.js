  define(['angular', 'appmodule'], function() {
	if ( ! window.console ) console = { log: function(){} }; 
	  
    var appModule = angular.module('app');

	appModule.filter('startFrom', function() {
		return function(input, start) {
			if(!input)return input;
			start = +start; //parse to int
			return input.slice(start);
		}
	});
	appModule.filter('range', function() {
		return function(input, total) {
			total = parseInt(total);
			for (var i=0; i<total; i++)
				input.push(i);
			return input;
		}
	});
	appModule.filter('language', function() {
		return function(input, language) {
			var out = [];
			//console.log("filter", input);
			//console.log("filter languageId", language);
			if(language){
				for (i in input){
					var schoolClass = input[i];
					if(schoolClass.languageId == language.id)out.push(schoolClass);
				}
			}else{
				out = input;
			}
			
			return out;
		}
	});
	
	appModule.filter('resourceType', function() {
		return function(input, typeId) {
			var out = [];
			//console.log("filter", typeId);
			//console.log("filter languageId", language);
			if(typeId == 0){
				out = input;
			}else if(input && typeId != 0){
				for (i in input){
					var resource = input[i];
					if(resource.resourceTypeId == typeId)out.push(resource);
				}
			}
			
			return out;
		}
	});
	
	appModule.filter('questionTypeId', function() {
		return function(input, questionTypeId) {
			var out = [];
			//console.log("filter", typeId);
			//console.log("filter languageId", language);
			if(!questionTypeId)
			{
				return;
			}
			if(input && questionTypeId != 0){
				for (i in input){
					var question = input[i];
					if(question.questionTypeId == questionTypeId)out.push(question);
				}
			}
			
			return out;
		}
	});
	
	
	
	appModule.filter('timecode', function() {
		return function(milliseconds) {
			if(!milliseconds)milliseconds = 0;
			duration = moment.duration(milliseconds);
			var hr = duration.hours().toString();
			if(hr.length == 1)hr = "0"+hr;
			var min = duration.minutes().toString();
			if(min.length == 1)min = "0"+min;
			var sec = duration.seconds().toString();
			if(sec.length == 1)sec = "0"+sec;
			return hr + ":" + min + ":" + sec;
		}
	});
	appModule.filter('admin', function() {
		return function(input, admin) {
			if(admin == 2){
				return input;
			}
			
			var out = [];
			
			for (i in input){
				var user = input[i];
				if(user.admin == admin)out.push(user);
			}
			return out;
		}
	});
	appModule.filter('accountArchived', function() {
		return function(input, archived) {
			if(archived == 2){
				return input;
			}
			
			var out = [];
			
			for (i in input){
				var user = input[i];
				if(user.archived == archived)out.push(user);
			}
			return out;
		}
	});

	/**
	 * Filter: pageRange(pagesArray, currentPage, totalPages)
	 * Filters the number of pages to return a group of pages that are 'near' the current page
	 * 
	 * @pagesArray  Array of pages
	 * @currentPage  The current selected page
	 * @totalPages  The total number of pages to filter
	 * @potentialPages  The total maximum number of pages we want to display at any one time
	 * @return  A limited set of pages that are 'near' the current selected page
	 */
	appModule.filter('pageRange', function() {
		return function (pagesArray, currentPage, totalPages, potentialPages) {
			currentPage = parseInt(currentPage);
			totalPages = parseInt(totalPages);
			potentialPages = parseInt(potentialPages);
			
			if (isNaN(totalPages) || totalPages < 1 || isNaN(currentPage) || currentPage < 0 || currentPage >= totalPages) {
				for (var i = 0; i < totalPages; i++)
					pagesArray.push(i);
				return pagesArray;
			}
			else
			{
				var numberOfPagesTotal = 0;
				
				// check potential number of pages in pager
				if (isNaN(potentialPages) || potentialPages < 1) {
					numberOfPagesTotal = 5;
				}
				else {
					if (potentialPages % 2 == 1) {
						numberOfPagesTotal = potentialPages;
					}
					else {
						numberOfPagesTotal = potentialPages + 1;
					}
				}
				
				if (totalPages <= numberOfPagesTotal) {
					for (var i = 0; i < totalPages; i++)
						pagesArray.push(i);
				}
				else if (totalPages > numberOfPagesTotal) {
					var numberOfPrePages = 0;
					var numberOfPostPages = 0;
					var numberOfPagesPerSide = (numberOfPagesTotal - 1) / 2;
					
					//console.log("numberOfPagesTotal == " + numberOfPagesTotal + ", numberOfPagesPerSide == " + numberOfPagesPerSide);
					
					pagesArray.push(currentPage);
					
					for (var i = currentPage - numberOfPagesPerSide; i <= currentPage + numberOfPagesPerSide; i++) {
						if (i < currentPage) {
							if (i >= 0) {
								pagesArray.push(i);
							}
							else {
								numberOfPostPages++;
							}
						}
						else if (i > currentPage) {
							if (i <= totalPages - 1) {
								pagesArray.push(i);
							}
							else {
								numberOfPrePages++;
							}
						}
					}
					
					//console.log("PrePages == " + numberOfPrePages + ", PostPages == " + numberOfPostPages);
					
					if (numberOfPrePages > 0) {
						//console.log("PrePages == " + numberOfPrePages + ", i == " + (currentPage - numberOfPagesPerSide - numberOfPrePages) + ", i < " + (currentPage - numberOfPagesPerSide));
						for (var i = currentPage - numberOfPagesPerSide - numberOfPrePages; i < currentPage - numberOfPagesPerSide; i++) {
							pagesArray.push(i);
						}
					}
					
					if (numberOfPostPages > 0) {
						//console.log("PostPages == " + numberOfPostPages + ", i == " + (currentPage + numberOfPagesPerSide) + ", i < " + (currentPage + numberOfPagesPerSide + numberOfPostPages));
						for (var i = currentPage + numberOfPagesPerSide; i < currentPage + numberOfPagesPerSide + numberOfPostPages; i++) {
							pagesArray.push(i + 1);
						}
					}
					
					pagesArray.sort( function(a, b) {return a - b} );
				}
				
				//console.log(pagesArray);
				return pagesArray;
			}
		}
	});
});

