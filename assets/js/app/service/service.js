define(['angular', 'appmodule', 'angular_resource'], function() {
	if ( ! window.console ) console = { log: function(){} };
    var appModule = angular.module('app');
	
	appModule.factory('User', ['$resource', function($resource){
		return $resource(site_url+'api/users/:userId', {userId:'@id'}, {
			get: {method:'GET', params:{}, isArray:false}
		});
	}]);
 
	
});
