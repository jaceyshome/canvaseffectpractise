define ["angular", "appmodule", "service/service"], ()->
	appModule = angular.module("app")
	appModule.controller( "CanvasRainCtrl",["$scope", ($scope)->

    window.requestAnimFrame = (callback)->
      window.requestAnimationFrame ||
      window.webkitRequestAnimationFrame ||
      window.mozRequestAnimationFrame ||
      window.oRequestAnimationFrame ||
      window.msRequestAnimationFrame ||
      (callback)->
        window.setTimeout(callback, 1000 / 24)

    onloadRainsBeforeHeros = ()->
      @canvas = document.getElementById("rainsBeforeHeros")
      @canvasWidth = @canvas.width
      @canvasHeight = @canvas.height
      @ctx = @canvas.getContext('2d')
      @dropTotal = 800
      @speed = 10
      @r = 1.2
      @h = 12
      @offset = 150
      @drops = Array()

      for i in [0..@dropTotal] by 1
        drop =
          x:Math.floor((Math.random()*@canvasWidth)-@offset)
          y:Math.floor((Math.random()*(@canvasHeight+1000))+1) * -1
          a: Math.floor((Math.random()*10)+8)*0.1
        @drops.push(drop)

    updateRainsBeforeHeros = ()->
      for drop in @drops
        if(drop.y > @canvasHeight  || drop.x > @canvasWidth)
          drop.x = Math.floor((Math.random()*@canvasWidth)-@offset)
          drop.y = Math.floor((Math.random()*@canvasHeight)+5) * -1
          drop.a = Math.floor((Math.random()*10)+8)*0.1
        drop.x += Math.cos(@r)*@speed*drop.a
        drop.y += Math.sin(@r)*@speed*drop.a

    drawRainsBeforeHeros = ()->
      @ctx.clearRect(0,0,@canvasWidth, @canvasHeight)
# TODO: add gradient
#      @ctx.strokeStyle="rgba(255, 255, 255, 0.5)"
#      @ctx.lineWidth = 1

      for drop in @drops
        @ctx.strokeStyle="rgba(180, 180, 180," + (drop.a - 0.2) + ")"
        @ctx.lineWidth = drop.a
        @ctx.beginPath()
        @ctx.moveTo(drop.x,drop.y)
        @ctx.lineTo(drop.x + @h*Math.cos(@r)*drop.a, drop.y + @h*Math.sin(@r)*drop.a)
        @ctx.stroke()


    onloadRainsMiddle = ()->
      @canvasMiddle = document.getElementById("rainsMiddle")
      @canvasMiddleWidth = @canvasMiddle.width
      @canvasMiddleHeight = @canvasMiddle.height
      @ctxMiddle = @canvasMiddle.getContext('2d')
      @dropMiddleTotal = 1100
      @speedMiddle = 8
      @rMid = 1.25
      @hMid= 15
      @offsetMid = 0
      @dropColor = 'white'
      @middleDrops = Array()

      for i in [0..@dropMiddleTotal] by 1
        drop =
          x:Math.floor((Math.random()*@canvasMiddleWidth)-@offsetMid)
          y:Math.floor((Math.random()*(@canvasMiddleHeight+1000))+1) * -1
          a:Math.floor((Math.random()*7.5)+4)*0.1
        @middleDrops.push(drop)

    updateRainsMiddle = ()->
      for drop in @middleDrops
        if(drop.y > @canvasMiddleHeight * drop.a || drop.x > @canvasMiddleWidth )
          drop.x = Math.floor((Math.random()*@canvasMiddleWidth)-@offsetMid)
          drop.y = Math.floor((Math.random()*@canvasMiddleHeight)+5) * -1
          drop.a = Math.floor((Math.random()*7.5)+4)*0.1
        drop.x += Math.cos(@rMid)*@speedMiddle*drop.a
        drop.y += Math.sin(@rMid)*@speedMiddle*drop.a

    drawRainsMiddle = ()->
      @ctxMiddle.clearRect(0,0,@canvasMiddleWidth, @canvasMiddleHeight)
      for drop in @middleDrops
        @ctxMiddle.strokeStyle="rgba(170, 170, 170," + drop.a + ")"
        @ctxMiddle.lineWidth = 1 * drop.a
        @ctxMiddle.beginPath()
        @ctxMiddle.moveTo(drop.x,drop.y)
        @ctxMiddle.lineTo(drop.x + @hMid*Math.cos(@rMid)*drop.a, drop.y + @hMid*Math.sin(@rMid)*drop.a)
        @ctxMiddle.stroke()

    onloadRainsBg = ()->
      @canvasBg = document.getElementById("rainsBg")
      @canvasBgWidth = @canvasBg.width
      @canvasBgHeight = @canvasBg.height
      @ctxBg = @canvasBg.getContext('2d')
      @dropBgTotal = 3000
      @speedBg = 6
      @rBg = 1.35
      @hBg = 15
      @offsetBg = -200
      @dropColor = 'white'
      @bgDrops = Array()

      for i in [0..@dropBgTotal] by 1
        drop =
          x:Math.floor((Math.random()*@canvasBgWidth)-@offsetBg)
          y:Math.floor((Math.random()*(@canvasBgHeight+1000))+1) * -1
          a: Math.floor((Math.random()*6.5)+2)*0.1
        @bgDrops.push(drop)

    updateRainsBg = ()->
      for drop in @bgDrops
        if(drop.y > @canvasBgHeight*drop.a  || drop.x > @canvasBgWidth)
          drop.x = Math.floor((Math.random()*@canvasBgWidth)-@offsetBg)
          drop.y = Math.floor((Math.random()*@canvasBgHeight)+5) * -1
          drop.a =  Math.floor((Math.random()*6.5)+2)*0.1
        drop.x += Math.cos(@rBg)*@speedBg*drop.a
        drop.y += Math.sin(@rBg)*@speedBg*drop.a

    drawRainsBg = ()->
      @ctxBg.clearRect(0,0,@canvasBgWidth, @canvasBgHeight)
      #      @ctxBg.strokeStyle="rgba(255, 255, 255, 0.5)"
      #      @ctxBg.lineWidth = 1

      for drop in @bgDrops
        @ctxBg.strokeStyle="rgba(234, 255, 238," + drop.a + ")"
        @ctxBg.lineWidth = 1 * drop.a
        @ctxBg.beginPath()
        @ctxBg.moveTo(drop.x,drop.y)
        @ctxBg.lineTo(drop.x + @hBg*Math.cos(@rBg)*drop.a, drop.y + @hBg*Math.sin(@rBg)*drop.a)
        @ctxBg.stroke()


# rain water
    onLoadRainwater = ()->
      @canvasRainWater = document.getElementById("rainwater")
      @ctxRainWater = @canvasRainWater.getContext('2d')
      @canvasRainWaterWidth = @canvasRainWater.width
      @canvasRainWaterHeight = @canvasRainWater.height
      @heightOffset = 200
      @rainWaterTotal = 500
      @rainWaterSpeed = 1.2
      @rainWaterColor = 'white'
      @rainWaters = Array()
      @rainWaterLife = 4
      for i in [0..@rainWaterTotal] by 1
        rainWater =
          x:Math.floor((Math.random()*@canvasRainWaterWidth)-5)
          y:Math.floor((Math.random()*(@canvasRainWaterHeight+50))+1)
          rOffset: Math.floor((Math.random()*3)-2)
          r:2
          count:Math.floor((Math.random()*2)-1)

        @rainWaters.push(rainWater)

    updateRainWater = ()->
      for rainWater in @rainWaters
        if(rainWater.count > @rainWaterLife)
          rainWater.x = Math.floor((Math.random()*@canvasRainWaterWidth)-5)
          rainWater.y = Math.floor((Math.random()*(@canvasRainWaterHeight + @heightOffset))+1)
          rainWater.rOffset = (Math.random()*2.2)-2
          rainWater.r = 2
          rainWater.count = Math.floor((Math.random()*2)-1)

        rainWater.count += 1
        rainWater.r += @rainWaterSpeed

    drawRainWater = ()->
      @ctxRainWater.clearRect(0,0,@canvasRainWaterWidth, @canvasRainWaterHeight + 1000)
      scaleX = 1
      scaleY = 0.8
      skewX = 0
      skewY = 0
      tx = 0
      ty = 0
      @ctxRainWater.setTransform(scaleX,skewX,skewY,scaleY,tx,ty)
      for rainWater in @rainWaters
        alpha = (rainWater.count * 2)/10
        @ctxRainWater.strokeStyle= "rgba(150, 150, 150," + alpha + ")"
        @ctxRainWater.lineWidth= Math.floor((Math.random()*10)+1)/10
        @ctxRainWater.beginPath()
        disRate =  rainWater.y / @canvasRainWaterHeight
        radius = (rainWater.r + rainWater.rOffset) * disRate
        @ctxRainWater.arc(rainWater.x,rainWater.y,radius,0,2*Math.PI, false)
        @ctxRainWater.stroke()

    animate =()->
      updateRainsBeforeHeros()
      drawRainsBeforeHeros()

      updateRainsMiddle()
      drawRainsMiddle()

      updateRainsBg()
      drawRainsBg()

      updateRainWater()
      drawRainWater()



      requestAnimationFrame(animate)


    window.onload = ()->
      onloadRainsBeforeHeros()
      onloadRainsMiddle()
      onloadRainsBg()
      onLoadRainwater()

      animate()


    window.onload()
	])
