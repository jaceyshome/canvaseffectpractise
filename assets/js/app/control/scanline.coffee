define ["angular", "appmodule", "service/service"], ()->
	appModule = angular.module("app")
	appModule.controller( "ScanlineCtrl",["$scope", ($scope)->

    window.requestAnimFrame = (callback)->
      window.requestAnimationFrame ||
      window.webkitRequestAnimationFrame ||
      window.mozRequestAnimationFrame ||
      window.oRequestAnimationFrame ||
      window.msRequestAnimationFrame ||
      (callback)->
        window.setTimeout(callback, 1000 / 24)

    window.onload = ()->
      onload()
      animate()

    onload =()->
      @canvas = document.getElementById("scanline")
      @ctx = @canvas.getContext('2d')
      @firstlineOffset = 0
      @speed = 0.5
      @imgWidth = 256
      @imgHeight = 256
      @rangeStart = 150
      @rangeEnd = 15

      @direction = -1
      @gap = 12
      @scanlineHeight = 1

      @img = new Image()
      @img.src = 'assets/images/scanline2.png'
#      @img.onload = drawImage()
      animate()

    drawImage =()->



      if(@rangeStart + @firstlineOffset > @rangeStart + @gap)
        @firstlineOffset = 0

      firstLinePosition = @rangeStart + @firstlineOffset

      @ctx.drawImage(@img, 0, 0)
      @imgData = @ctx.getImageData(0,0,@imgWidth,@imgHeight)
#      console.log @imgData

      for l in [firstLinePosition..rangeEnd]  by @gap * @direction
        currentY = l
        for y in [currentY..(currentY+scanlineHeight)] by 1
          inPost = y * @imgWidth * 4
          endPost = inPost + @imgWidth * 4
          for x in [inPost..endPost] by 1
            @imgData.data[x] = r = @imgData.data[x++]
            @imgData.data[x] = g = @imgData.data[x++]
            @imgData.data[x] = b = @imgData.data[x++] * 1.02
            @imgData.data[x] = a = @imgData.data[x++]

#      isAddNoise = (Math.floor(Math.random()*10 + 1) ) % 5
      if (Math.floor(Math.random()*10 + 1) ) % 5
        addNoise()

      @ctx.putImageData(@imgData, 0, 0)

    addNoise = ()->
      bandWith = 6
      startPosition =  (@rangeStart - @rangeEnd ) * 0.5
      endPosition =  (@rangeStart - @rangeEnd ) * 0.3
      for y in [endPosition..startPosition]  by 1
        offset = Math.floor((Math.random()*bandWith )+ 1) * 4
        inPost = y * @imgWidth * 4
        endPost = inPost + @imgWidth * 4
        for x in [inPost..endPost] by 1
          if(x+offset > endPost)
            @imgData.data[x] = 0
          @imgData.data[x] = @imgData.data[x+offset]

    animate =()->
      update()
      requestAnimationFrame(animate)

    update =()->
      @ctx.clearRect(0,0,@imgWidth,@imgHeight)
      drawImage()
      @firstlineOffset += @speed

    window.onload()
	])
