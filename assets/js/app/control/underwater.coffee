define ["angular", "appmodule", "service/service"], ()->
	appModule = angular.module("app")
	appModule.controller( "UnderWaterCtrl",["$scope", ($scope)->

		window.requestAnimFrame = (callback)->
			window.requestAnimationFrame ||
			window.webkitRequestAnimationFrame ||
			window.mozRequestAnimationFrame ||
			window.oRequestAnimationFrame ||
			window.msRequestAnimationFrame ||
			(callback)->
				window.setTimeout(callback, 1000 / 24)

		window.onload = ()->
	#      onLoadBgImage()
			canvas = document.getElementById("canvasScene")
			@ctx = canvas.getContext('2d')
			@stageWidth = 1024
			@stageHeight = 660

#			onLoadBubbleFront()
			onLoadSunLine()
#			animate()

		onLoadBgImage = ()->
			@img = new Image()
			@img.src = 'assets/images/scanline2.png'
			@img.onload = drawBgImage()

		drawBgImage = ()->
			@ctx.drawImage(@img, 0, 0)

#			bubbles

		onLoadBubbleFront = ()->
			@frontBubbleGroups  = Array()
			@bubbleConfigFront =
				groupMax : 2
				groupHeightMax: 20
				groupHeightMin:10
				groupWidthMax:10
				groupWidthMin:5
				maxNum : 20
				heightMax : 10
				heightMin : 1
				widthMax : 10
				widthMin : 1
				speedMax: 40
				distance:100
				maxHeight:700
				maxWidth:1024
				angle:0

			@frontBubbleGroups = initBubbleGroups(@bubbleConfigFront)
			drawBubbleGroups(@frontBubbleGroups)

		initBubbleGroups = (config)->
			bubbleGroups = new Array()
			for i in [0..config.groupMax] by 1
				addBubbleGroup(config, bubbleGroups)
			return bubbleGroups

		addBubbleGroup = (config, bubbleGroups)->
			bubbleGroupX = Math.floor((Math.random()*10) + 0) * 100
			bubbleGroupY = Math.floor((Math.random()*10) + 0) * 20
			bubbles = new Array()
			for b in [0..config.maxNum] by 1
				offsetX = Math.floor((Math.random()*config.groupWidthMax)+ config.groupWidthMin) * Math.floor((Math.random()*config.groupHeightMax)+ config.groupHeightMin) * 0.14
				offsetY = Math.floor((Math.random()*config.groupHeightMax)+ config.groupHeightMin) * Math.floor((Math.random()*config.groupHeightMax)+ config.groupHeightMin)
				_width = Math.floor((Math.random()*config.widthMax)+ config.widthMin)
				_height = Math.floor((Math.random()*config.heightMax)+ config.heightMin)

				bubble =
					x: offsetX + bubbleGroupX
					y: offsetY + bubbleGroupY + @stageHeight
					width : _width
					height : _height
					speed: config.speedMax / (_width+_height)

				bubbles.push(bubble)
			bubbleGroups.push(bubbles)

		destoryBubbleGroup = (bubbleGroup, bubbleGroups)->
			for i in [0..bubbleGroups.length] by 1
				if(bubbleGroups[i] == bubbleGroup)
					bubbleGroups.splice(i,1)

		drawBubbleGroups = (bubbleGroups)->
			if bubbleGroups == undefined
				return
			for g in [0..bubbleGroups.length - 1] by 1
				bubbles = bubbleGroups[g]
				for b in [0..bubbles.length - 1] by 1
					bubble = bubbles[b]
					drawBubble(bubble)

		drawBubble = (bubble)->
			if bubble == undefined
				return
			@ctx.strokeStyle="rgba(0, 0, 0, 0.2)"
			@ctx.lineWidth = 1
			@ctx.beginPath()

			startX = bubble.x + bubble.width / 2;
			startY = bubble.y + bubble.height / 2;
			startR = bubble.height / 2;
			endX = bubble.x + bubble.width;
			endY = bubble.y + bubble.height;
			endR =   bubble.height / 4;
			gradient = @ctx.createRadialGradient(startX, startY, startR,endX,endY, endR)
			gradient.addColorStop(0,"rgba(213,240,247,0.3)");
			gradient.addColorStop(0.3, "rgba(17,37,61,0.6)");

			@ctx.moveTo(bubble.x,bubble.y)

			x1 = bubble.x
			y1 = bubble.y + bubble.height / 2
			x2 = bubble.x + bubble.width
			y2 = y1 + (Math.random()*4.5 - 0.2)
			x = x2
			y = bubble.y

			@ctx.bezierCurveTo(x1,y1,x2,y2,x,y)
			@ctx.moveTo(bubble.x,bubble.y)

			x1 = bubble.x
			y1 = bubble.y - bubble.height / 2
			x2 = bubble.x + bubble.width
			y2 = y1 + (Math.random()*4 - 0.2)
			x = x2
			y = bubble.y

			@ctx.bezierCurveTo(x1,y1,x2,y2,x,y)
			@ctx.stroke()
			@ctx.fillStyle = gradient
			@ctx.fill()

		updateBubbleGroups =(bubbleGroups)->
			for i in [0..bubbleGroups.length-1] by 1
				bubbles = bubbleGroups[i]
				destoryBubbleGroupSignal = true
				for j in [0..bubbles.length-1] by 1
					bubble = bubbles[j]
					updateBubble(bubble)
					if(bubble.y >= 0)
						destoryBubbleGroupSignal = false
				if(destoryBubbleGroupSignal)
					destoryBubbleGroup(bubbles, bubbleGroups)
					addBubbleGroup(@bubbleConfigFront,bubbleGroups)

		updateBubble =(bubble)->
			bubble.y -= bubble.speed
#			console.log(bubble.y)




#		sun line


















		animate =()->
			@ctx.clearRect(0,0,@stageWidth, @stageHeight)
			updateBubbleGroups(@frontBubbleGroups)
			drawBubbleGroups(@frontBubbleGroups)
			requestAnimationFrame(animate)

		window.onload()
	])
